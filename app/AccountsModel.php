<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountsModel extends Model
{
    protected $table = 'user_account';
    protected $fillable = [
        'id',
        'wallet_name', 
        'account_number',
        'mobile_number',
        'email',
        'date',
        'myMoney'
    ];
    public function create($request)
    {
        
        $randnum = rand(1111111111,9999999999);
        $addAccount = new AccountsModel;
        $addAccount->account_id = Auth::user()->id;
        $addAccount->wallet_name = $request->wallet_name;
        $addAccount->account_number = $randnum;
        $addAccount->mobile_number = $request->mobile_number;
        $addAccount->email = $request->email;
        $addAccount->date = date("Y-m-d");

        if ($addAccount->save()) {
            return redirect()->back();
        }
    }
    public function updateData($request)
    {
        
        $updateData = AccountsModel::findorFail($request->id);
        $updateData->wallet_name = $request->wallet_name;
        $updateData->mobile_number = $request->mobile_number;
        $updateData->email = $request->email;

        if ($updateData->save()) {
            return redirect()->back();
        }
    }
    public function deleteData($id)
    {
        
        $data = AccountsModel::where('id', $id)->delete();
        return redirect()->back();
    }
}
