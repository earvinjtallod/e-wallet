<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AccountsModel;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $myaccounts = \DB::table('user_account')->where('account_id',Auth::user()->id)->get();
        return view('home')->with('myaccounts', $myaccounts);
    }
}
