<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AccountsModel;

class MywalletController extends Controller
{
    public function store(Request $request)
    {
        $accounts = new AccountsModel;
        return $accounts->create($request);    
    }
    public function view($id)
    {
        $data = \DB::table('user_account')->where('id',$id)->first();    
        return view('updateWallet')->with('data',$data);
    }
    public function postUpdate(Request $request)
    {
        $accountsModel = new AccountsModel;
        return $accountsModel->updateData($request);
    }
    public function postDelete($id)
    {
        $accountsModel = AccountsModel::where('id', $id)->delete();
        return response()->json([
            'status' => 'Success'
        ]);
    }
}
