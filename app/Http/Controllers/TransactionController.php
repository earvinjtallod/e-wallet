<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransactionModel;
use Auth;

class TransactionController extends Controller
{
    public function viewTransactionForm($id)
    {
        $data = \DB::table('user_account')->where('id',$id)->first();    
        return view('viewTransactionForm')->with('data',$data);
    }
    public function transction(Request $request)
    {
        $transactionModel = new TransactionModel;
        return $transactionModel->postTransaction($request);
    }
    public function viewTransction()
    {
        $myTransactions = \DB::table('transactions')->where('account_id',Auth::user()->id)->get();
        $checkOfIncomingAmount = \DB::table('transactions')->where('account_id',Auth::user()->id)->where('type','incoming')->where('status','success')->sum('amount');
        $checkOfOutcomingAmount = \DB::table('transactions')->where('account_id',Auth::user()->id)->where('type','outgoing')->where('status','success')->sum('amount');
        $sumOfIncomingAmount = (!empty($checkOfIncomingAmount) ? $checkOfIncomingAmount : 0.00);
        $sumOfOutcomingAmount = (!empty($checkOfOutcomingAmount) ? $checkOfOutcomingAmount : 0.00);
        return view('myTransactions')->with('myTransactions', $myTransactions)->with('sumOfIncomingAmount',$sumOfIncomingAmount)->with('sumOfOutcomingAmount',$sumOfOutcomingAmount);
    }
    public function deleteTransaction($id)
    {
       $transactionData = TransactionModel::where('id', $id)->delete();
        return response()->json([
            'status' => 'Success'
        ]);
    }
    public function viewTransactionDetails($id)
    {
        $data = \DB::table('transactions')->where('id',$id)->first();    
        return view('viewTransactionDetails')->with('data',$data);
    }
    public function postFraudTransaction(Request $request)
    {
        
        $transactionModel = new TransactionModel;
        return $transactionModel->reportFraud($request);
    }
}
