<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class TransactionModel extends Model
{
    protected $table = 'transactions';
    protected $fillable = [
        'id',
        'account_id',
        'transaction_number',
        'sender_account_number', 
        'receiver_account_number',
        'sender_name',
        'receiver_name',
        'amount',
        'date',
        'status',
        'type'
    ];

    public function postTransaction($request)
    {
        $validator = \Validator::make($request->all(), [
            'receiver_accnum' => 'required',
            'amount' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json([
              'status' => 'failed'
            ]);
        }

        //verify receivers account number and details
        $receiverDetails = \DB::table('user_account')->where('account_number',$request->receiver_accnum)->first();
        if (empty($receiverDetails)) {
            return response()->json([
              'status' => 'invalid account'
            ]);
        }
        
        //check sender's fund
        $senderDetails = \DB::table('user_account')->where('account_number',$request->sender_accnum)->first();
        if ($senderDetails->myMoney < $request->amount) {
            return response()->json([
              'status' => 'insufficient funds'
            ]);
        }

        //check if sender account and receiver account is equal
        if ($request->receiver_accnum == $senderDetails->account_number) {
            return response()->json([
              'status' => 'transaction not allowed'
            ]);
        }

        //transactoion number
        $transaction_number = rand(0000000000,9999999999);
        //update sender's fund
        $totalfund = $senderDetails->myMoney - $request->amount;
        $updateData = AccountsModel::findorFail($senderDetails->id);
        $updateData->myMoney = $totalfund;
        $account_id = Auth::user()->id;
        $type = 'outgoing';
        $account_number = $senderDetails->account_number;
        $this->transactionLogs($transaction_number,$account_number,$senderDetails,$receiverDetails,$request->amount,$type,$account_id);
        
        if ($updateData->save()) {
            //update receiver's fund
            $account_id = $receiverDetails->account_id;
            $totalamount = $receiverDetails->myMoney + $request->amount;
            $updatereceiveraccount = AccountsModel::findorFail($receiverDetails->id);
            $updatereceiveraccount->myMoney = $totalamount;
            $type = 'incoming';
            $account_number = $receiverDetails->account_number;
            $this->transactionLogs($transaction_number,$account_number,$senderDetails,$receiverDetails,$request->amount,$type,$account_id);
            
            if ($updatereceiveraccount->save()) {
                return response()->json([
                    'status' => 'success'
                ]);
            }
        }
    }

    public function transactionLogs($transaction_number,$account_number,$senderDetails,$receiverDetails,$amount,$type,$id) 
    {
        $addTransaction = new TransactionModel;
        $addTransaction->account_id = $id;
        $addTransaction->transaction_number = $transaction_number;
        $addTransaction->account_number = $account_number;
        $addTransaction->sender_account_number = $senderDetails->account_number;
        $addTransaction->receiver_account_number = $receiverDetails->account_number;
        $addTransaction->sender_name = $senderDetails->wallet_name;
        $addTransaction->receiver_name = $receiverDetails->wallet_name;
        $addTransaction->amount = number_format($amount, 2);
        $addTransaction->status = "Success";
        $addTransaction->type = $type;
        $addTransaction->date = date("Y-m-d");
        $addTransaction->save();

    }

    public function reportFraud($request)
    {
        $getTransactionData = \DB::table('transactions')->where('id',$request->id)->first();
        $amount = $getTransactionData->amount;
        $getReceiverAccount = \DB::table('user_account')->where('account_number',$getTransactionData->receiver_account_number)->first();
        $getSenderAccount = \DB::table('user_account')->where('account_number',$getTransactionData->sender_account_number)->first();
        
        $totalamount = $getReceiverAccount->myMoney - $amount;
        $updatereceiveraccount = AccountsModel::findorFail($getReceiverAccount->id);
        $updatereceiveraccount->myMoney = $totalamount;
        $updatereceiveraccount->save();
        //update senders fund
        $totalfund = $getSenderAccount->myMoney + $amount;
        $updateData = AccountsModel::findorFail($getSenderAccount->id);
        $updateData->myMoney = $totalfund;
        $updateData->save();
        $getTransactionStatuses = \DB::table('transactions')->where('transaction_number',$getTransactionData->transaction_number)->get();

        foreach ($getTransactionStatuses as $getTransactionStatus) {
            
            $updateTransaction = TransactionModel::findorFail($getTransactionStatus->id);
            $updateTransaction->status = "fraud";
            $updateTransaction->save();
        }

       

        return response()->json([
            'status' => 'fraud'
        ]);

    }

    


}
