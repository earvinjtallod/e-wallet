@extends('layouts.main')

@section('content')
<div class="profile_page" id="transactionTable">
	<div class="row">
		<div class="col-lg-12">
			<div class="tab-pane" id="contacts" role="tabpanel">
                <div class="row">
                    <div class="col-xl-3">
                        <div class="card">
                            <div class="card-header contact-user">
                                <h5 class="mb-0">John Doe</h5>
                            </div>
                            <div class="card-block">
                                <ul class="list-group list-contacts">
                                    <li class="list-group-item active"><a href="#">Transactions</a></li>
                                    <li class="list-group-item"><b>Incoming:</b>  ₱{{ number_format($sumOfIncomingAmount,2) }}</li>
                                    <li class="list-group-item"><b>Outcoming:</b> ₱{{number_format($sumOfOutcomingAmount,2)}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9">
                        <div class="card" >
                            <div class="card-body">
                                <div class="table-responsive datatable-primary data_table_main table-responsive dt-responsive">
                                    <table id="dataTable2" class="table-striped text-center dataTable no-footer dtr-inline">
                                        <thead class="text-capitalize">
                                        <tr>
                                            <th>Wallet Name</th>
                                            <th>Account Number</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($myTransactions))
                                        @foreach($myTransactions as $myTransaction)
                                        <tr>
                                            <td>{{ $myTransaction->sender_name }}</td>
                                            <td>{{ $myTransaction->account_number }}</td>
                                            
                                            <td>{{ number_format($myTransaction->amount, 2) }}</td>
                                            <td>{{ $myTransaction->date }}</td>
                                            <td>{{ $myTransaction->type }}</td>
                                            <td>{{ $myTransaction->status }}</td>
                                            <td><button class="btn btn-danger" onclick="deleteTransaction({{$myTransaction->id}})"><i class="ti-trash"></i></button>
                                                @if($myTransaction->status != "fraud" )
                                                <button class="btn btn-info" data-target="#transactionModal" onclick="viewTransaction({{$myTransaction->id}})" data-toggle="modal"><i class="ti-location-arrow"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<!-- modal for reporting transaction as fraud -->
<div class="modal fade" id="transactionModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Transaction</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
            </div>
            <div class="modal-body" id="transactionDetailsBody">
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function deleteTransaction(id)
    {
        $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ url('delete-transaction') }}/" + id,
        dataType: 'json',
            success: function(response) {
                $("#transactionTable").load(location.href+" #transactionTable>*","");
                toastr.success('Success', 'Account has been deleted');
           }
        });

        return false;

}
</script>

<script type="text/javascript">
    function viewTransaction(id)
    {
       var url = "/view-transaction-details/" + id;
        $.ajax({
           url: url,
           type: 'get',            
           beforeSend: function() {
            $('#transactionDetailsBody').html('');
           },
           success: function(response) {
            $('#transactionDetailsBody').html(response);
           }
        });

        return false;

}
</script>

@endsection