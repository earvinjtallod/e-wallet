<form method="post" action="{{ url('/edit-wallet') }}">
    @csrf
    <div class="form-group">
        <label for="name" class="col-form-label">Wallet Name</label>
        <input class="form-control" type="text" id="wallet_name" name="wallet_name" value="{{ $data->wallet_name }}">
        <input class="form-control" type="hidden" id="id" name="id" value="{{ $data->id }}" readonly>
    </div>
    <div class="form-group">
        <label for="email" class="col-form-label">Email</label>
        <input class="form-control" type="email" id="email" name="email" value="{{ $data->email }}">
    </div>
    <div class="form-group">
        <label for="mobile_number" class="col-form-label">Mobile Number</label>
        <input class="form-control" type="tel" id="mobile_number" name="mobile_number" value="{{ $data->mobile_number }}">
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
    </div>
</form>