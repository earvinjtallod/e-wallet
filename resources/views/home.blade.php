@extends('layouts.main')

@section('content')

    <div class="row" id="divMyAccounts">
        <div class="col-lg-12 stretched_card mt-mob-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card_title">My Wallet</h4>
                    <button type="button" class="btn btn-success mb-3 pull-right" data-toggle="modal" data-target="#CreateWalletModal"><i class="ti-reload"></i> Add Account</button>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-striped text-center">
                                <thead class="text-uppercase">
                                <tr>
                                    <th scope="col">Account Number</th>
                                    <th scope="col">Wallet Name</th>
                                    <th scope="col">Wallet Balance</th>
                                    <th scope="col">action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($myaccounts))
                                    @foreach($myaccounts as $myaccount)
                                        <tr>
                                            <td>{{$myaccount->account_number}}</td>
                                            <td>{{$myaccount->wallet_name}}</td>
                                            <td>{{number_format($myaccount->myMoney, 2)}}</td>
                                            <td>
                                                <button class="btn btn-success" data-target="#EditWalletModal" data-toggle="modal" onclick="getWalletData({{$myaccount->id}})"><i class="ti-pencil"></i></button>
                                                <button class="btn btn-danger" onclick="deleteData({{$myaccount->id}})"><i class="ti-trash"></i></button>
                                                <button class="btn btn-info" data-target="#transactionModal" onclick="createTransaction({{$myaccount->id}})" data-toggle="modal"><i class="ti-location-arrow"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- modal for creating virtual wallet -->
    <div class="modal fade" id="CreateWalletModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create Wallet</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ url('/post-wallet') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="col-form-label">Wallet Name</label>
                            <input class="form-control" type="text" id="wallet_name" name="wallet_name">
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-form-label">Email</label>
                            <input class="form-control" type="email" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="mobile_number" class="col-form-label">Mobile Number</label>
                            <input class="form-control" type="tel" id="mobile_number" name="mobile_number">
                        </div>
                </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    <!-- end modal -->
    <!-- modal for edit virtual wallet -->
    <div class="modal fade" id="EditWalletModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create Wallet</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body" id="editWalletBody">
                    
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->
    <!-- modal for transaction -->
<div class="modal fade" id="transactionModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Transaction</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
            </div>
            <div class="modal-body" id="transactionFormBody">
                
            </div>
        </div>
    </div>
</div>

<!-- script for fetch wallet data -->
<script type="text/javascript">
    function getWalletData(id)
    {
        var url = "/view-wallet/" + id;
        $.ajax({
           url: url,
           type: 'get',            
           beforeSend: function() {
            $('#editWalletBody').html('');
           },
           success: function(response) {
            $('#editWalletBody').html(response);
           }
        });

        return false;

}
</script>

<script type="text/javascript">
    function deleteData(id)
    {
        $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ url('delete-wallet') }}/" + id,
        dataType: 'json',
            success: function(response) {
                $("#divMyAccounts").load(location.href+" #divMyAccounts>*","");
                toastr.success('Success', 'Account has been deleted');
           }
        });

        return false;

}
</script>

<script type="text/javascript">
    function createTransaction(id)
    {
       var url = "/view-transaction-form/" + id;
        $.ajax({
           url: url,
           type: 'get',            
           beforeSend: function() {
            $('#transactionFormBody').html('');
           },
           success: function(response) {
            $('#transactionFormBody').html(response);
           }
        });

        return false;

}
</script>
@endsection
