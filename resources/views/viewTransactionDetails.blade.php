<form id="transactionDetails">
    @csrf
    <div class="form-group">
        <label for="name" class="col-form-label">Receiver's Account Number</label>
        <input class="form-control" type="number" id="receiver_accnum" name="receiver_accnum" value="{{ $data->account_number }}" readonly="">
        <input class="form-control" type="hidden" id="id" name="id" value="{{ $data->id }}" readonly="">
    </div>
    <div class="form-group">
        <label for="email" class="col-form-label">Amount</label>
        <input class="form-control" type="number" id="amount" name="amount" step="any" value="{{ number_format($data->amount, 2) }}" readonly="">
    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-danger">Fraud</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
    </div>
</form>


<script type="text/javascript">

    $("#transactionDetails").submit(function(event){
    event.preventDefault();

        var values = new FormData(this);
        var url = "/post-fraud-transaction";

        $.ajax({
            url: url,
            type: "post",
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            processData: false,
            cache: false,
            contentType: false,
            data: values ,
            success: function (response) {
                if(response.status == 'fraud') {
                    $('#transactionModal').modal('hide');
                    $("#transactionTable").load(location.href+" #transactionTable>*","");
                    toastr.success('Mark as Fraud', 'Success');
                }
            }
        });
    });
</script>