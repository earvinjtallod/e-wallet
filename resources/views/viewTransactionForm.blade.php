<form id="transactionForm">
    @csrf
    <div class="form-group">
        <label for="name" class="col-form-label">Receiver's Account Number</label>
        <input class="form-control" type="number" id="receiver_accnum" name="receiver_accnum">
        <input class="form-control" type="hidden" id="sender_accnum" name="sender_accnum" value="{{ $data->account_number }}">
    </div>
    <div class="form-group">
        <label for="email" class="col-form-label">Amount</label>
        <input class="form-control" type="number" id="amount" name="amount" step="any">
    </div>

    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
    </div>
</form>


<script type="text/javascript">

    $("#transactionForm").submit(function(event){
    event.preventDefault();

        var values = new FormData(this);
        var url = "/post-transaction";

        $.ajax({
            url: url,
            type: "post",
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            processData: false,
            cache: false,
            contentType: false,
            data: values ,
            success: function (response) {
                if(response.status == 'insufficient funds') {
                    toastr.error('Insufficient Funds', 'Failed');
                } else if(response.status == 'failed') {
                    toastr.error('Please complete the form', 'Failed');
                } else if(response.status == 'invalid account') {
                    toastr.error('Invalid Account', 'Failed');
                } else if(response.status == 'transaction not allowed') {
                    toastr.error('Transaction Not Allowed', 'Failed');
                } else if(response.status == 'success') {
                    $('#transactionModal').modal('hide');
                    $("#divMyAccounts").load(location.href+" #divMyAccounts>*","");
                    toastr.success('Transaction Complete!', 'Success');
                }
               
            }
            
        });

    });
</script>

