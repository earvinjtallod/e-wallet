<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>

    <!--=========================*
                Met Data
    *===========================-->
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Falr - Bootstrap 4 Admin Dashboard Template">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--=========================*
              Page Title
    *===========================-->
    <title>Falr - Bootstrap 4 Admin Dashboard Template</title>

    <!--=========================*
                Favicon
    *===========================-->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.png') }}">

    <!--=========================*
            Bootstrap Css
    *===========================-->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">

    <!--=========================*
              Custom CSS
    *===========================-->
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

    <!--=========================*
               Owl CSS
    *===========================-->
    <link href="{{ URL::asset('css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('css/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css">

    <!--=========================*
            Font Awesome
    *===========================-->
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}">

    <!--=========================*
             Themify Icons
    *===========================-->
    <link rel="stylesheet" href="{{ URL::asset('css/themify-icons.css') }}">

    <!--=========================*
               Ionicons
    *===========================-->
    <link href="{{ URL::asset('css/ionicons.min.css') }}" rel="stylesheet"/>

    <!--=========================*
              EtLine Icons
    *===========================-->
    <link href="{{ URL::asset('css/et-line.css') }}" rel="stylesheet"/>

    <!--=========================*
              Feather Icons
    *===========================-->
    <link href="{{ URL::asset('css/feather.css') }}" rel="stylesheet"/>

    <!--=========================*
              Flag Icons
    *===========================-->
    <link href="{{ URL::asset('css/flag-icon.min.css') }}" rel="stylesheet"/>

    <!--=========================*
               Toastr Css
    *===========================-->

    <link rel="stylesheet" href="{{ URL::asset('vendors/toastr/css/toastr.min.css') }}">

    <!--=========================*
              Modernizer
    *===========================-->
    <script src="{{ URL::asset('js/modernizr-2.8.3.min.js') }}"></script>

    <!--=========================*
               Datatable
    *===========================-->
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendors/data-table/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendors/data-table/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendors/data-table/css/responsive.bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('vendors/data-table/css/responsive.jqueryui.min.css') }}">



    <!--=========================*
               Slick Menu
    *===========================-->
    <link rel="stylesheet" href="{{ URL::asset('css/slicknav.min.css') }}">
    
    <!--=========================*
              Flag Icons
    *===========================-->
    <link href="{{ URL::asset('css/flag-icon.min.css') }}" rel="stylesheet"/>

    <!--=========================*
               AM Chart
    *===========================-->
    <link rel="stylesheet" href="{{ URL::asset('vendors/am-charts/css/am-charts.css') }}" type="text/css" media="all" />

    <!--=========================*
               Morris Css
    *===========================-->
    <link rel="stylesheet" href="{{ URL::asset('vendors/charts/morris-bundle/morris.css') }}">

    <!--=========================*
            Google Fonts
    *===========================-->

    <!-- Font USE: 'Roboto', sans-serif;-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">

    <!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

        <!-- Jquery Js -->
        <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!--=========================*
         Page Container
*===========================-->
<div id="page-container" class="light-sidebar">

    <!--==================================*
               Header Section
    *====================================-->
    @include("layouts.include.header")    
    <!--==================================*
               End Header Section
    *====================================-->

    <!--=========================*
             Side Bar Menu
    *===========================-->
    @include("layouts.include.navbar")   
    <!--=========================*
           End Side Bar Menu
    *===========================-->

    <!--==================================*
               Main Content Section
    *====================================-->
    <div class="main-content page-content">

        <!--==================================*
                   Main Section
        *====================================-->
        <div class="main-content-inner">
           @yield('content')
        </div>
        <!--==================================*
                   End Main Section
        *====================================-->
    </div>
    <!--=================================*
           End Main Content Section
    *===================================-->

    <!--=================================*
                  Footer Section
    *===================================-->
    @include("layouts.include.footer")
    <!--=================================*
                End Footer Section
    *===================================-->

</div>
<!--=========================*
        End Page Container
*===========================-->


<!--=========================*
            Scripts
*===========================-->


<!-- bootstrap 4 js -->
<script src="{{ URL::asset('js/popper.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- Owl Carousel Js -->
<script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
<!-- SlimScroll Js -->
<script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
<!-- Slick Nav -->
<script src="{{ URL::asset('js/jquery.slicknav.min.js') }}"></script>
<!-- ========== This Page js ========== -->

<!-- start amchart js -->
<script src="{{ URL::asset('vendors/am-charts/js/ammap.js') }}"></script>
<script src="{{ URL::asset('vendors/am-charts/js/worldLow.js') }}"></script>
<script src="{{ URL::asset('vendors/am-charts/js/continentsLow.js') }}"></script>
<script src="{{ URL::asset('vendors/am-charts/js/light.js') }}"></script>
<!-- maps js -->
<script src="{{ URL::asset('js/am-maps.js') }}"></script>

<!--Float Js-->
<script src="{{ URL::asset('vendors/charts/float-bundle/jquery.flot.js') }}"></script>
<script src="{{ URL::asset('vendors/charts/float-bundle/jquery.flot.pie.js') }}"></script>
<script src="{{ URL::asset('vendors/charts/float-bundle/jquery.flot.resize.js') }}"></script>

<!--Chart Js-->
<script src="{{ URL::asset('vendors/charts/charts-bundle/Chart.bundle.js') }}"></script>

<!--Apex Chart-->
<script src="{{ URL::asset('vendors/apex/js/apexcharts.min.js') }}"></script>

<!--toastr-->
<script src="{{URL::asset('vendors/toastr/js/toastr.min.js') }}"></script>

<!--html2canvas-->
<script src="{{URL::asset('vendors/html2canvas/dist/html2canvas.min.js') }}"></script>

<!--jspdf-->
<script src="{{URL::asset('vendors/jspdf/dist/jspdf.debug.js') }}"></script>

<!--Home Script-->
<script src="{{ URL::asset('js/home.js') }}"></script>

<!-- Data Table js -->
<script src="{{ URL::asset('vendors/data-table/js/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('vendors/data-table/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('vendors/data-table/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('vendors/data-table/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('vendors/data-table/js/responsive.bootstrap.min.js') }}"></script>


<!-- ========== This Page js ========== -->
<!-- Data table Init -->
<script src="js/init/data-table.js"></script>


<!-- Main Js -->
<script src="{{ URL::asset('js/main.js') }}"></script>



</body>
</html>
