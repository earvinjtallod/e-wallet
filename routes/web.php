<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//post created child account
Route::post('/post-wallet', 'MywalletController@store');
//edit wallet
Route::get('/view-wallet/{id}', 'MywalletController@view');
Route::post('/edit-wallet', 'MywalletController@postUpdate');
//delete wallet
Route::post('/delete-wallet/{id}', 'MywalletController@postDelete');
//acount details
Route::get('/my-transaction', 'TransactionController@viewTransction')->name('myTransaction');
//view transaction modal
Route::get('/view-transaction-form/{id}', 'TransactionController@viewTransactionForm');
//post transaction
Route::post('/post-transaction', 'TransactionController@transction');
//delete transaction
Route::post('/delete-transaction/{id}', 'TransactionController@deleteTransaction');
//view my transaction details
Route::get('/view-transaction-details/{id}', 'TransactionController@viewTransactionDetails');
//post fraud transaction
Route::post('/post-fraud-transaction', 'TransactionController@postFraudTransaction');



