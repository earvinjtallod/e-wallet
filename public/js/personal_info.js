jQuery(document).ready(function () {
// remember tab
$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('ActiveTab', $(e.target).attr('href'));
});
var ActiveTab = localStorage.getItem('ActiveTab');
if(ActiveTab){
    $('#Tabs a[href="' + ActiveTab + '"]').tab('show');
}

})
var NumberOfDependentInput = 1

// adding dependent input
$("#AddNewDependentInput").on('click',function(){
    NumberOfDependentInput ++;
    CurrentNumberOfDependentInput = NumberOfDependentInput - 1
    $("#AddDependentForm").removeClass(' was-validated')
    var NewInput = $('#DependentInput1').clone();

    $(NewInput).attr("id","DependentInput"+NumberOfDependentInput);
    $(NewInput).find('#NumberOfDependentInput').text(NumberOfDependentInput+".");
    $(NewInput).find('input:not([type=hidden])').val('')

    $(NewInput).find("#DependentFirstName").attr("name",'DependentFirstName'+NumberOfDependentInput);
    $(NewInput).find("#DependentLastName").attr("name",'DependentLastName'+NumberOfDependentInput);
    $(NewInput).find("#DependentDateOfBirth").attr("name",'DependentDateOfBirth'+NumberOfDependentInput);
    $(NewInput).find("#DependentGender").attr("name",'DependentGender'+NumberOfDependentInput);
    $(NewInput).find("#DependentAddress").attr("name",'DependentAddress'+NumberOfDependentInput);
    $(NewInput).find("#RelationToDependent").attr("name",'RelationToDependent'+NumberOfDependentInput);

    $(NewInput).insertAfter("#DependentInput"+CurrentNumberOfDependentInput);

    $("#NumberOfDependent").val(NumberOfDependentInput)
})

//Removing dependent input
$("#RemoveDependentInput").on('click',function(){
    if (NumberOfDependentInput == 1){

    }else{
        $('#DependentInput'+NumberOfDependentInput).remove();
        NumberOfDependentInput --;
        $("#NumberOfDependent").val(NumberOfDependentInput)
    }
    
    
})

















